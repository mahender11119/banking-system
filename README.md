App:
port: 8081

MongoDB:
1. download mongodb-windows-x86_64-4.4.3-signed.exe
2. install it
3. default port : 27017
4. create database with name bankingsystem

Keycloak:
1. Download keycloak-12.0.2 zip file extract to a folder
2. Go to bin folder and run below command, server will be started.
   Linux/Unix
	$ cd bin
	$ ./standalone.sh
   Windows
	> ...\bin\standalone.bat
3. Create admin account (port is 8180)
	i. Open http://localhost:8180/auth in your web browser.
	ii. Enter a username and password to create an initial admin user
4. Create realm with name BankingSystem
5. Create client with name bs-eltropy
	i. In settings tab select Access Type as confidential
	ii. Click save.
	iii. Go to credential and take Secret key (ex: 051f992d-7d60-4c47-9c42-223032ffbf3f)
6. Create admin role (rolename = admin)
7. Create employee role (rolename = employee)
8. Create user with name bs-admin
	i. Go to Credentials tab and set
		password = test@123
		Temporary = OFF
	ii. Go to Role Mappings tab assign admin role
	

Postman collection for the APIs:

1.	Generate jwt token
   http://localhost:8180/auth/realms/BankingSystem/protocol/openid-connect/token
   Method: POST
   Request Params:
   grant_type = password
   client_id = bs-eltropy
   client_secret = 051f992d-7d60-4c47-9c42-223032ffbf3f
   username = employee1
   password = test@123
   
   Response:
   {
    "access_token": "eyJhbGciOiJSUzI1NiIsInR5cC",
    "expires_in": 300,
    "refresh_expires_in": 1800,
    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cC",
    "token_type": "Bearer",
    "not-before-policy": 0,
    "session_state": "4f735ea8-5907-4277-b4fc-67d064498732",
    "scope": "email profile"
	}
	
2.	Sign in as admin/employee.
	http://localhost:8081/api/v1/employee/login
	Method: POST
	Headers: Bearer token
	Request Body:
	{
		"username":"employee1",
		"password":"test@123"
	}
	
3. Sign out as admin/employee.
	http://localhost:8081/api/v1/employee/logout
	Method: POST
	Headers: Bearer token
	Request Body:
	{
		"refresh_token":"eyJhbGciOiJSUzI1NiIsInR5cC"
	}
	
	Response Body:
	{
		"Logged out successfully"
	}
	
4. Add bank employee
	http://localhost:8081/api/v1/employee
	Method: POST
	Headers: Bearer token
	Request Body:
	{
		"name":"testemp",
		"salary":"10000",
		"jobRole":"employee",
		"firstName":"Test",
		"lastName":"emp",
		"email":"test@my.com",
		"username":"testemp",
		"password":"test@123"
	}
	Response Body:
	{
		"name":"testemp",
		"salary":"10000",
		"jobRole":"employee",
		"id":"601955d887914e6736880670",
		"username":"testemp"
	}

5. Delete employee
	http://localhost:8081/api/v1/employee/601955d887914e6736880670
	Method: DELETE
	Headers: Bearer token
	
6. Create a customer
	http://localhost:8081/api/v1/customer
	Method: POST
	Headers: Bearer token
	Request Body:
	{
		"street":"downtown",
		"city":"Hyd",
		"state":"ts",
		"firstName":"Test",
		"lastName":"emp",
		"zip":"500020"
	}
	Response Body:
	{
		"id":"601955d887914e6736880670",
		"street":"downtown",
		"city":"Hyd",
		"state":"ts",
		"firstName":"Test",
		"lastName":"emp",
		"zip":"500020"
		"accountId":null
		"kyc":null
	}
	
7. Create account
	http://localhost:8081/api/v1/account
	Method: POST
	Headers: Bearer token
	Request Body:
	{
		"type":"SAVINGS",
		"description":"savings account",
		"balance":5000,
		"customerId":"601955d887914e6736880670"
	}
	Response Body:
	{
		"id":"601955d887914e6736880670",
		"type":"SAVINGS",
		"description":"savings account",
		"balance":5000,
		"customerId":"601955d887914e6736880670"
	}
	
8. Link customer with account
	http://localhost:8081/api/v1/customer/link/customer-account
	Method: POST
	Headers: Bearer token
	Request Body:
	{
		"account-id":"601955d887914e6736880670",
		"customer-id":"60194893ff011274db6e815a"
	}
	Response Body:
	{
		"id":"601955d887914e6736880670",
		"street":"downtown",
		"city":"Hyd",
		"state":"ts",
		"firstName":"Test",
		"lastName":"emp",
		"zip":"500020",
		"accountId":"601955d887914e6736880670",
		"kyc":null
	}
	
9. Update KYC for a customer
	http://localhost:8081/api/v1/customer/update/kyc
	Method: POST
	Headers: Bearer token
	Request Body:
	{
		"kyc":"4e6736880670",
		"customer-id":"60194893ff011274db6e815a"
	}
	Response Body:
	{
		"id":"601955d887914e6736880670",
		"street":"downtown",
		"city":"Hyd",
		"state":"ts",
		"firstName":"Test",
		"lastName":"emp",
		"zip":"500020",
		"accountId":"601955d887914e6736880670",
		"kyc":"4e6736880670"
	}

10. Get details of a customer
	http://localhost:8081/api/v1/customer/60194893ff011274db6e815a
	Method: GET
	Headers: Bearer token
	Response Body:
	{
		"id":"601955d887914e6736880670",
		"street":"downtown",
		"city":"Hyd",
		"state":"ts",
		"firstName":"Test",
		"lastName":"emp",
		"zip":"500020",
		"accountId":"601955d887914e6736880670",
		"kyc":"4e6736880670"
	}
	
11. Delete customer
	http://localhost:8081/api/v1/customer/60194893ff011274db6e815a
	Method: DELETE
	Headers: Bearer token
	
12. Get account balance for an account
	http://localhost:8081/api/v1/account/balance/601955d887914e6736880670
	Method: GET
	Headers: Bearer token
	Respone: 5000
	
13. Transfer money from one account to another
	http://localhost:8081/api/v1/account/transfer/amount
	Method: POST
	Headers: Bearer token
	Request Body:
	{
		"from-account":"601955d887914e6736880670",
		"to-account":"60194893ff01122e4323e85a",
		"transfer-amount":"2000"
	}
	Response :
	Amount transferred successfully
	
14. Print Account statement of an account for a time range in pdf
	http://localhost:8081/api/v1/account/export-statement/pdf
	Method: GET
	Headers: Bearer token
	Request Body:
	{
		"fromdate":"01-02-2020 11:11:11",
		"toDate":"02-03-2021 11:11:11",
		"accountId":"601955d887914e6736880670"
	}
	Response :
	Pdf file
	
15. Calculate interest for the money annually (at 3.5% p.a.) and update the account balance.
	http://localhost:8081/api/v1/account/calculate-Interest/annual
	Method: POST
	Headers: Bearer token
	Request Body:
	{
		"id":"601955d887914e6736880670"
	}
	Response Body:
	{
		"id":"601955d887914e6736880670",
		"type":"SAVINGS",
		"description":"savings account",
		"balance":5000,
		"customerId":"601955d887914e6736880670",
		"interestCalculatedOn":"02-03-2021 11:11:11"
	}
