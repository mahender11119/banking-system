package com.bs.api.constants;

public interface RestConstants {

	String API = "api";
	String V1 = "v1";
	String SLASH = "/";
	String EMPLOYEE = "employee";
	String CUSTOMER = "customer";
	String ACCOUNT = "account";
	String EXPORT_STATEMENT = "export-statement";
	String PDF = "pdf";
	String CALCULATE_INTEREST = "calculate-Interest";
	String ANNUAL = "annual";
	
	String EMPLOYEE_PATH = SLASH + EMPLOYEE;
	String CUSTOMER_PATH = SLASH + CUSTOMER;
	String ACCOUNT_PATH = SLASH + ACCOUNT;
	String API_PATH = SLASH + API + SLASH + V1;
	
	String EXPORT_STATEMENT_PATH = SLASH + EXPORT_STATEMENT;
	String PDF_PATH = SLASH + PDF;
	String ACCOUNT_PDF_PATH = EXPORT_STATEMENT_PATH + PDF_PATH;
	String ANNUAL_PATH = SLASH + ANNUAL;
	String CALCULATE_INTEREST_PATH = SLASH + CALCULATE_INTEREST;
	String CALCULATE_INTEREST_ANNUAL_PATH = CALCULATE_INTEREST_PATH + ANNUAL_PATH;
}
