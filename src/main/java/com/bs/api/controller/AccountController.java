package com.bs.api.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bs.api.entity.Account;
import com.bs.api.entity.Transaction;
import com.bs.api.exception.AccountNotMappingException;
import com.bs.api.exception.ResourceNotFoundException;
import com.bs.util.UserPDFExporter;
import com.lowagie.text.DocumentException;
import com.bs.api.service.AccountService;

import static com.bs.api.constants.RestConstants.ACCOUNT_PATH;
import static com.bs.api.constants.RestConstants.API_PATH;
import static com.bs.api.constants.RestConstants.ACCOUNT_PDF_PATH;
import static com.bs.api.constants.RestConstants.CALCULATE_INTEREST_ANNUAL_PATH;

@RestController
@RequestMapping(API_PATH + ACCOUNT_PATH)
public class AccountController {

	@Autowired
	AccountService accountService;

	private static final Logger logger = LoggerFactory
			.getLogger(AccountController.class);

	@ApiOperation(value = "Creation of bank account")
	@RolesAllowed("employee")
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Account create(@RequestBody Account account) {
		logger.info("Creating account with {}.", account);
		return accountService.create(account);
	}

	@ApiOperation(value = "Get account balance")
	@RolesAllowed("employee")
	@GetMapping(value = "/balance/{id}")
	public Long getBalance(
			@ApiParam("Account Number") @PathVariable("id") String id)
			throws ResourceNotFoundException {
		logger.info("Getting account balance with Id: {}.", id);
		return accountService.getBalance(id);
	}

	@ApiOperation(value = "Generate account statement for the give period")
	@RolesAllowed("employee")
	@GetMapping(ACCOUNT_PDF_PATH)
	public void printStatement(@RequestParam() String accountId,
			@RequestParam() String fromdate, @RequestParam() String todate,
			HttpServletResponse response) throws DocumentException, IOException {
		logger.info("Get statement from {} to {}.", fromdate, todate);
		response.setContentType("application/pdf");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime
				+ ".pdf";
		response.setHeader(headerKey, headerValue);
		List<Transaction> listUsers = accountService.printStatement(accountId,
				fromdate, todate);

		UserPDFExporter exporter = new UserPDFExporter(listUsers);
		exporter.export(response);
	}

	@ApiOperation(value = "Calculate interest annually")
	@RolesAllowed("employee")
	@PostMapping(CALCULATE_INTEREST_ANNUAL_PATH)
	public Account calculateInterest(@RequestBody Account account)
			throws Exception {
		logger.info("Calculate interest with {}.", account);
		String accountId = account.getId();
		return accountService.calculateInterest(accountId);
	}

	@ApiOperation(value = "Transfer amount from one account to another account")
	@RolesAllowed("employee")
	@PostMapping("/transfer/amount")
	public ResponseEntity<?> transferAmount(
			@RequestBody Map<String, String> accountInfo)
			throws ResourceNotFoundException, AccountNotMappingException {
		String fromAccount = accountInfo.get("from-account");
		String toAccount = accountInfo.get("to-account");
		String transferAmount = accountInfo.get("transfer-amount");
		logger.info("Transfer amount {} from account {} to account {}.",
				transferAmount, fromAccount, toAccount);
		accountService.transferAmount(transferAmount, fromAccount, toAccount);
		return ResponseEntity.ok().body("Amount transferred successfully");
	}
}
