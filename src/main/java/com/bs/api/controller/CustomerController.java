package com.bs.api.controller;

import java.util.Map;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bs.api.entity.Customer;
import com.bs.api.exception.ResourceNotFoundException;
import com.bs.api.request.CustomerRequest;
import com.bs.api.service.CustomerService;
import com.bs.util.MappingUtils;

import static com.bs.api.constants.RestConstants.CUSTOMER_PATH;
import static com.bs.api.constants.RestConstants.API_PATH;

@RestController
@RequestMapping(API_PATH + CUSTOMER_PATH)
public class CustomerController {

	@Autowired
	CustomerService customerService;

	@ApiOperation(value = "Add customer")
	@RolesAllowed("employee")
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Customer create(@RequestBody CustomerRequest customerRequest) {
		Customer customer = MappingUtils
				.customerRequestToCustomer(customerRequest);
		return customerService.create(customer);
	}

	@ApiOperation(value = "Get customer by id")
	@RolesAllowed("employee")
	@GetMapping(value = "/{id}")
	public ResponseEntity<Customer> find(
			@ApiParam("Customer Id") @PathVariable("id") String id)
			throws ResourceNotFoundException {
		Customer customer = customerService.find(id);
		return ResponseEntity.ok().body(customer);
	}

	@ApiOperation(value = "Delete customer by id")
	@RolesAllowed("employee")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> delete(
			@ApiParam("Customer Id") @PathVariable("id") String id)
			throws ResourceNotFoundException {
		customerService.delete(id);
		return ResponseEntity.ok().body("Customer deleted successfully");
	}

	@ApiOperation(value = "Link customer with account")
	@RolesAllowed("employee")
	@PostMapping(value = "/link/customer-account")
	public ResponseEntity<Customer> linkCustomerWithAccount(
			@RequestBody Map<String, String> customerAccount)
			throws ResourceNotFoundException {
		String accountId = customerAccount.get("account-id");
		String customerId = customerAccount.get("customer-id");
		Customer customer = customerService.linkCustomerWithAccount(accountId,
				customerId);
		return ResponseEntity.ok().body(customer);
	}

	@ApiOperation(value = "Update KYC for Customer")
	@RolesAllowed("employee")
	@PostMapping(value = "/update/kyc")
	public ResponseEntity<Customer> updateKycForCustomer(
			@RequestBody Map<String, String> kycInfo)
			throws ResourceNotFoundException {
		String kyc = kycInfo.get("kyc");
		String customerId = kycInfo.get("customer-id");
		Customer customer = customerService.updateKycForCustomer(kyc,
				customerId);
		return ResponseEntity.ok().body(customer);
	}
}
