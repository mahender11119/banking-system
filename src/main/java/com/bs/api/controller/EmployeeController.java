package com.bs.api.controller;

import java.util.Map;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bs.api.entity.Employee;
import com.bs.api.exception.ResourceNotFoundException;
import com.bs.api.request.EmployeeRequest;
import com.bs.api.service.EmployeeService;

import static com.bs.api.constants.RestConstants.EMPLOYEE_PATH;
import static com.bs.api.constants.RestConstants.API_PATH;

@RestController
@RequestMapping(API_PATH + EMPLOYEE_PATH)
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@ApiOperation(value = "Add employee")
	@RolesAllowed("admin")
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Employee create(@RequestBody EmployeeRequest employee) {
		return employeeService.create(employee);
	}

	@ApiOperation(value = "Delete employee by id")
	@RolesAllowed("admin")
	@DeleteMapping(value = "/{id}")
	public void delete(@ApiParam("Employee Id") @PathVariable("id") String id)
			throws ResourceNotFoundException {
		employeeService.delete(id);
	}

	@RolesAllowed({ "admin", "employee" })
	@PostMapping(path = "/login")
	public ResponseEntity<String> login(
			@RequestBody Map<String, String> credentials)
			throws ServletException {
		String username = credentials.get("username");
		String password = credentials.get("password");
		String token = employeeService.login(username, password);
		return ResponseEntity.ok(token);
	}

	@RolesAllowed({ "admin", "employee" })
	@PostMapping(path = "/logout")
	public ResponseEntity<String> logout(
			@RequestBody Map<String, String> credentials)
			throws ServletException {
		String refreshToken = credentials.get("refresh_token");
		employeeService.logout(refreshToken);
		return ResponseEntity.ok("Logged out successfully");
	}
}
