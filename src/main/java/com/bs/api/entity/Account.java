package com.bs.api.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

@Document(collection = "accounts")
public class Account {
	@Id
	String id;
	String type;
	String description;
	Long balance;
	@JsonProperty(required = true)
	String customerId;
	Date interestCalculatedOn;
	public Account() {
		// TODO Auto-generated constructor stub
	}

	public Account(String type, String description, Long balance, String customerId, Date interestCalculatedOn) {
		super();
		this.type = type;
		this.description = description;
		this.balance = balance;
		this.customerId = customerId;
		this.interestCalculatedOn = interestCalculatedOn;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getBalance() {
		return balance;
	}

	public void setBalance(Long balance) {
		this.balance = balance;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Date getInterestCalculatedOn() {
		return interestCalculatedOn;
	}

	public void setInterestCalculatedOn(Date interestCalculatedOn) {
		this.interestCalculatedOn = interestCalculatedOn;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", type=" + type + ", description="
				+ description + ", balance=" + balance + ", customerId="
				+ customerId + ", interestCalculatedOn=" + interestCalculatedOn
				+ "]";
	}
	
}
