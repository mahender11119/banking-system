package com.bs.api.entity;

public enum AccountEnum {

	SAVINGS, SALARY, LOAN, CURRENT;
}
