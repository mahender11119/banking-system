package com.bs.api.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "employees")
public class Employee {

	@Id
	String id;
	String name;
	String salary;
	String jobRole;
	String userName;
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public Employee(String name, String salary, String jobRole, String userName) {
		super();
		this.name = name;
		this.salary = salary;
		this.jobRole = jobRole;
		this.userName = userName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getJobRole() {
		return jobRole;
	}

	public void setJobRole(String jobRole) {
		this.jobRole = jobRole;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", salary=" + salary
				+ ", jobRole=" + jobRole + ", userName=" + userName + "]";
	}
	
}
