package com.bs.api.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "transactions")
public class Transaction {

	@Id
	String id;
	Date dateTime;
	String accountId;
	Long amount;
	Long balance;
	
	public Transaction() {
		// TODO Auto-generated constructor stub
	}

	public Transaction(Date dateTime, String accountId, Long amount,
			Long balance) {
		super();
		this.dateTime = dateTime;
		this.accountId = accountId;
		this.amount = amount;
		this.balance = balance;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Long getBalance() {
		return balance;
	}

	public void setBalance(Long balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Transaction [id=" + id + ", dateTime=" + dateTime
				+ ", accountId=" + accountId + ", amount=" + amount
				+ ", balance=" + balance + "]";
	}
	
}
