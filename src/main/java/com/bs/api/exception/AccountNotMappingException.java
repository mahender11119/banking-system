package com.bs.api.exception;

public class AccountNotMappingException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public AccountNotMappingException(String message) {
		super(message);
	}
}
