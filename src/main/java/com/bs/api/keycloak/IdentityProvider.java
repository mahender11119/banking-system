package com.bs.api.keycloak;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import org.keycloak.representations.AccessTokenResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.bs.api.request.EmployeeRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class IdentityProvider {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	KecloakProperties kecloakProperties;

	private static final Logger logger = LoggerFactory
			.getLogger(IdentityProvider.class);

	public static final String KEYCLOAK_ADMIN_TOKEN_URI = "http://localhost:8180/auth/realms/master/protocol/openid-connect/token";

	public static final String ADD_USER = "http://localhost:8180/auth/admin/realms/BankingSystem/users";

	public static final String ALL_USERS = "http://localhost:8180/auth/admin/realms/BankingSystem/users";

	public static final String EMPLOYEE_ROLE_ID = "462e15f6-bd3e-4044-b309-8a66eae2835f";

	public void addUser(EmployeeRequest employee) {
		String token = adminToken().getToken();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(token);
		Map<String, Object> requestData = new HashMap<>();
		requestData.put("firstName", employee.getFirstName());
		requestData.put("lastName", employee.getLastName());
		requestData.put("email", employee.getEmail());
		requestData.put("username", employee.getUsername());
		requestData.put("enabled", "true");
		HttpEntity<Map<String, Object>> request = new HttpEntity<>(requestData,
				headers);
		restTemplate.postForObject(ADD_USER, request, String.class);
		String userId = getUserId(employee.getUsername(), token);
		updatePassword(userId, employee.getPassword(), token);
		updateUserRole(userId, token);
	}

	public void deleteUser(String userName) {
		String token = adminToken().getToken();
		String userId = getUserId(userName, token);
		String deleteUserUri = "http://localhost:8180/auth/admin/realms/BankingSystem/users/"
				+ userId;
		HttpHeaders headers = new HttpHeaders();
		headers.setBearerAuth(token);
		HttpEntity request = new HttpEntity(headers);
		ResponseEntity<String> response = restTemplate.exchange(deleteUserUri,
				HttpMethod.DELETE, request, String.class);
		String body = response.getBody();
	}

	public String getUserId(String userName, String token) {
		String userId = "";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setBearerAuth(token);
		HttpEntity request = new HttpEntity(headers);
		ResponseEntity<String> response = restTemplate.exchange(ALL_USERS,
				HttpMethod.GET, request, String.class);
		String body = response.getBody();
		ObjectMapper mapper = new ObjectMapper();
		List<Map<String, Object>> data = new ArrayList<>();
		try {
			data = mapper.readValue(body,
					new TypeReference<List<Map<String, Object>>>() {
					});
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (Map<String, Object> userData : data) {
			Object usr = userData.get("username");
			if (usr != null && usr.toString().equalsIgnoreCase(userName)) {
				userId = userData.get("id") != null ? userData.get("id")
						.toString() : "";
				break;
			}
		}
		return userId;
	}

	private void updatePassword(String userId, String password, String token) {
		String updatePswdUri = "http://localhost:8180/auth/admin/realms/BankingSystem/users/"
				+ userId + "/reset-password";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(token);
		Map<String, Object> requestData = new HashMap<>();
		requestData.put("type", "password");
		requestData.put("value", password);
		requestData.put("temporary", false);
		HttpEntity<Map<String, Object>> request = new HttpEntity<>(requestData,
				headers);
		restTemplate.put(updatePswdUri, request);
	}

	private void updateUserRole(String userId, String token) {
		String roleUri = "http://localhost:8180/auth/admin/realms/BankingSystem/users/"
				+ userId + "/role-mappings/realm";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setBearerAuth(token);
		// Map<String, Object> requestData = new HashMap<>();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("id", EMPLOYEE_ROLE_ID);
		jsonObject.put("name", "employee");
		JSONArray array = new JSONArray();
		array.add(jsonObject);
		HttpEntity<JSONArray> request = new HttpEntity<>(array, headers);
		restTemplate.postForObject(roleUri, request, String.class);
	}

	/**
	 * This method will call keycloak service to admin login. after successful
	 * login it will provide access token.
	 */
	private AccessTokenResponse adminToken() {
		try {
			MultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
			requestParams.add("client_id", "admin-cli");
			requestParams.add("username", "adminuser");
			requestParams.add("password", "test@123");
			requestParams.add("grant_type", "password");
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(
					requestParams, headers);
			AccessTokenResponse keycloakAccessToken = getAccessTokenResponse(
					request, KEYCLOAK_ADMIN_TOKEN_URI);
			return keycloakAccessToken;
		} catch (Exception e) {
			logger.error("login **** ", e.getMessage());
			throw e;
		}
	}

	/**
	 * This method will call keycloak service to user login. after successful
	 * login it will provide access token.
	 */
	public AccessTokenResponse login(String username, String password) {
		try {
			MultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
			requestParams.add("client_id", kecloakProperties.getClientId());
			requestParams.add("username", username);
			requestParams.add("password", password);
			requestParams.add("grant_type", kecloakProperties.getGrantType());
			requestParams.add("client_secret",
					kecloakProperties.getClientSecret());
			// requestParams.add("scope", "openid");
			AccessTokenResponse keycloakAccessToken = queryKeycloakByParams(requestParams);

			return keycloakAccessToken;
		} catch (Exception e) {
			logger.error("login in keycloak ", e);
			throw e;
		}
	}

	private AccessTokenResponse queryKeycloakByParams(
			MultiValueMap<String, String> requestParams) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(
				requestParams, headers);
		// String url =
		// "http://localhost:8080/auth/realms/dev/protocol/openid-connect/token";
		AccessTokenResponse keycloakAccessToken = getAccessTokenResponse(
				request, kecloakProperties.getTokenUri());
		return keycloakAccessToken;
	}

	private AccessTokenResponse getAccessTokenResponse(
			HttpEntity<MultiValueMap<String, String>> request, String url) {
		try {
			ResponseEntity<AccessTokenResponse> response = restTemplate
					.postForEntity(url, request, AccessTokenResponse.class);
			return response.getBody();
		} catch (ResourceAccessException e) {
			logger.error("KeyCloak getAccessTokenResponse: ", e.getMessage());
			try {
				ResponseEntity<AccessTokenResponse> response = restTemplate
						.postForEntity(url, request, AccessTokenResponse.class);
				return response.getBody();
			} catch (Exception ex) {
				throw ex;
			}
		} catch (Exception e) {
			throw e;
		}
	}

	public void logout(String refreshToken) {
		try {
			MultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
			requestParams.add("client_id", kecloakProperties.getClientId());
			requestParams.add("client_secret",
					kecloakProperties.getClientSecret());
			requestParams.add("refresh_token", refreshToken);
			logoutUserSession(requestParams);
		} catch (Exception e) {
			logger.error("KeyCloak logout: ", e.getMessage());
			throw e;
		}
	}

	private void logoutUserSession(MultiValueMap<String, String> requestParams) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(
				requestParams, headers);
		restTemplate.postForEntity(kecloakProperties.getLogoutUri(), request,
				Object.class);
	}
}
