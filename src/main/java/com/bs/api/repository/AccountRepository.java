package com.bs.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.bs.api.entity.Account;

@Repository
public interface AccountRepository extends MongoRepository<Account, String>{
}
