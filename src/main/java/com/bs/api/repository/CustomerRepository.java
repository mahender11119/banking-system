package com.bs.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.bs.api.entity.Customer;

@Repository
public interface CustomerRepository extends MongoRepository<Customer, String>{

}
