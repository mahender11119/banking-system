package com.bs.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.bs.api.entity.Employee;
@Repository
public interface EmployeeRepository extends MongoRepository<Employee, String>{

}
