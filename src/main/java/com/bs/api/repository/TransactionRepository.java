package com.bs.api.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.bs.api.entity.Transaction;

@Repository
public interface TransactionRepository extends MongoRepository<Transaction, String>{

	List<Transaction> findByAccountIdAndDateTimeBetween(String accountId, Date startDate, Date endDate);
}
