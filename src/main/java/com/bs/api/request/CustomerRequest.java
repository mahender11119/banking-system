package com.bs.api.request;

public class CustomerRequest {
	String lastName;
	String firstName;
	String street;
	String city;
	String state;
	String zip;
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	@Override
	public String toString() {
		return "CustomerRequest [lastName=" + lastName + ", firstName="
				+ firstName + ", street=" + street + ", city=" + city
				+ ", state=" + state + ", zip=" + zip + "]";
	}
	
}
