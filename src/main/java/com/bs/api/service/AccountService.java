package com.bs.api.service;




import java.util.List;

import com.bs.api.entity.Account;
import com.bs.api.entity.Transaction;
import com.bs.api.exception.AccountNotMappingException;
import com.bs.api.exception.ResourceNotFoundException;

public interface AccountService {

	Account create(Account account);
	
	Long getBalance(String id) throws ResourceNotFoundException;
	
	List<Transaction> printStatement(String accountId, String fromDate, String toDate);
	
	Account calculateInterest(String accountId) throws ResourceNotFoundException, Exception;
	
	void transferAmount(String transferAmount, String fromAccount, String toAccount) throws ResourceNotFoundException, AccountNotMappingException;
}
