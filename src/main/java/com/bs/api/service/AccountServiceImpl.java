package com.bs.api.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bs.api.entity.Account;
import com.bs.api.entity.Transaction;
import com.bs.api.exception.AccountNotMappingException;
import com.bs.api.exception.InSufficientFundException;
import com.bs.api.exception.ResourceNotFoundException;
import com.bs.api.repository.AccountRepository;
import com.bs.util.BankCalculation;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	TransactionService transactionService;

	private static final Logger logger = LoggerFactory
			.getLogger(AccountServiceImpl.class);

	@Override
	public Account create(Account account) {
		account.setInterestCalculatedOn(new Date());
		return accountRepository.save(account);
	}

	@Override
	public Long getBalance(String id) throws ResourceNotFoundException {
		Account account = accountRepository.findById(String.valueOf(id))
				.orElseThrow(
						() -> new ResourceNotFoundException(
								"Customer not found with id " + id));
		return account.getBalance();
	}

	@Override
	public List<Transaction> printStatement(String accountId, String fromDate, String toDate) {
		SimpleDateFormat formatter = new SimpleDateFormat(
				"dd-MM-yyyy HH:mm:ss");
		List<Transaction> listOfTransactions = new ArrayList<>();
		try {
			listOfTransactions = transactionService.findByDateTimeBetween(
					accountId, formatter.parse(fromDate), formatter.parse(toDate));
		} catch (ParseException e) {
			logger.error("dates are not proper ",e);
		}
		return listOfTransactions;
	}

	@Override
	public Account calculateInterest(String accountId) throws Exception {
		Date today = new Date();
		Account accountData = accountRepository.findById(
				String.valueOf(accountId)).orElseThrow(
				() -> new ResourceNotFoundException(
						"Account not found with id " + accountId));
		long actualBalance = accountData.getBalance();
		Date lastCalculatedDate = accountData.getInterestCalculatedOn();
		long difference_In_Time = today.getTime()
				- lastCalculatedDate.getTime();
		long difference_In_Days = (difference_In_Time / (1000 * 60 * 60 * 24));
		if (difference_In_Days < 365) {
			throw new Exception("Interest will be calculated annually");
		}
		long interest = BankCalculation
				.calculateInterestAnnually(actualBalance);
		actualBalance = actualBalance + interest;
		accountData.setBalance(actualBalance);
		accountData.setInterestCalculatedOn(today);
		return accountRepository.save(accountData);
	}

	@Override
	public void transferAmount(String transferAmount, String fromAccount,
			String toAccount) throws ResourceNotFoundException,
			AccountNotMappingException {
		long amount = Double.valueOf(transferAmount).longValue();
		Account fromAccntDetails = accountRepository.findById(fromAccount)
				.orElseThrow(
						() -> new ResourceNotFoundException(
								"Account not found with id " + fromAccount));
		Account toAccntDetails = accountRepository.findById(toAccount)
				.orElseThrow(
						() -> new ResourceNotFoundException(
								"Account not found with id " + toAccount));
		if (fromAccntDetails.getCustomerId() == null
				|| fromAccntDetails.getCustomerId() == "") {
			logger.info("Account {} is not mapped with customer.",
					fromAccntDetails.getId());
			throw new AccountNotMappingException(String.format(
					"Account %s is not mapped with customer",
					fromAccntDetails.getId()));
		}
		if (toAccntDetails.getCustomerId() == null
				|| toAccntDetails.getCustomerId() == "") {
			logger.info("Account {} is not mapped with customer.",
					toAccntDetails.getId());
			throw new AccountNotMappingException(String.format(
					"Account %s is not mapped with customer",
					toAccntDetails.getId()));
		}
		Long fromAccountBal = fromAccntDetails.getBalance();
		Long toAccountBal = toAccntDetails.getBalance();
		if (amount > fromAccountBal) {
			logger.info("Current balance {} is less than requested amount {}.",
					fromAccountBal, amount);
			throw new InSufficientFundException(String.format(
					"Current balance %d is less than requested amount %d",
					fromAccountBal, amount));
		}
		fromAccountBal = fromAccountBal - amount;
		toAccountBal = toAccountBal + amount;
		fromAccntDetails.setBalance(fromAccountBal);
		accountRepository.save(fromAccntDetails);

		// performing transaction insert
		Transaction transaction = new Transaction();
		transaction.setAccountId(toAccntDetails.getId());
		transaction.setAmount(amount);
		transaction.setBalance(toAccountBal);
		transaction.setDateTime(new Date());

		transactionService.create(transaction);
		toAccntDetails.setBalance(toAccountBal);
		accountRepository.save(toAccntDetails);
	}
}
