package com.bs.api.service;

import com.bs.api.entity.Customer;
import com.bs.api.exception.ResourceNotFoundException;

public interface CustomerService {

	Customer create(Customer customer);
	
	Customer find(String id) throws ResourceNotFoundException;
	
	void delete(String id) throws ResourceNotFoundException;
	
	Customer linkCustomerWithAccount(String accountId,String customerId) throws ResourceNotFoundException;
	
	Customer updateKycForCustomer(String kyc,String customerId) throws ResourceNotFoundException;
}
