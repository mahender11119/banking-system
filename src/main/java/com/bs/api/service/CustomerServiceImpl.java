package com.bs.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bs.api.exception.ResourceNotFoundException;
import com.bs.api.entity.Account;
import com.bs.api.entity.Customer;
import com.bs.api.repository.AccountRepository;
import com.bs.api.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	AccountRepository accountRepository;

	@Override
	public Customer create(Customer customer) {
		return customerRepository.save(customer);
	}

	@Override
	public Customer find(String id) throws ResourceNotFoundException {
		Customer customer = customerRepository.findById(String.valueOf(id))
				.orElseThrow(
						() -> new ResourceNotFoundException(
								"Customer not found with id " + id));
		return customer;
	}

	@Override
	public void delete(String id) throws ResourceNotFoundException {
		Customer customer = customerRepository.findById(String.valueOf(id))
				.orElseThrow(
						() -> new ResourceNotFoundException(
								"Customer not found with id " + id));
		customerRepository.delete(customer);
	}

	@Override
	public Customer linkCustomerWithAccount(String accountId, String customerId)
			throws ResourceNotFoundException {
		Customer customer = customerRepository.findById(customerId)
				.orElseThrow(
						() -> new ResourceNotFoundException(
								"Customer not found with id " + customerId));
		customer.setAccountId(accountId);
		Account account = accountRepository.findById(accountId).orElseThrow(
				() -> new ResourceNotFoundException(
						"Account not found with id " + accountId));
		account.setCustomerId(customerId);
		accountRepository.save(account);
		return customerRepository.save(customer);
	}

	@Override
	public Customer updateKycForCustomer(String kyc, String customerId)
			throws ResourceNotFoundException {
		Customer customer = customerRepository.findById(
				String.valueOf(customerId)).orElseThrow(
				() -> new ResourceNotFoundException(
						"Customer not found with id " + customerId));
		customer.setKyc(kyc);
		return customerRepository.save(customer);
	}

}
