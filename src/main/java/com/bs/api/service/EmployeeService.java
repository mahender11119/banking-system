package com.bs.api.service;

import javax.servlet.ServletException;

import com.bs.api.entity.Employee;
import com.bs.api.exception.ResourceNotFoundException;
import com.bs.api.request.EmployeeRequest;

public interface EmployeeService {
	Employee create(EmployeeRequest employee);
	void delete(String id) throws ResourceNotFoundException;
	String login(String username, String password) throws ServletException;
	void logout(String refreshToken) throws ServletException;
}
