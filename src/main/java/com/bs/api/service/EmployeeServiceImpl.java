package com.bs.api.service;

import javax.servlet.ServletException;

import org.keycloak.representations.AccessTokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bs.api.entity.Employee;
import com.bs.api.exception.ResourceNotFoundException;
import com.bs.api.keycloak.IdentityProvider;
import com.bs.api.repository.EmployeeRepository;
import com.bs.api.request.EmployeeRequest;
import com.bs.util.MappingUtils;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	IdentityProvider identityProvider;

	@Override
	public Employee create(EmployeeRequest employeeRequest) {
		Employee employee = MappingUtils
				.employeeRequestToEmployee(employeeRequest);
		identityProvider.addUser(employeeRequest);
		return employeeRepository.save(employee);
	}

	@Override
	public void delete(String id) throws ResourceNotFoundException {
		Employee employee = employeeRepository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException(
						"Customer not found with id " + id));
		employeeRepository.delete(employee);
		identityProvider.deleteUser(employee.getUserName());
	}

	@Override
	public String login(String username, String password)
			throws ServletException {
		AccessTokenResponse accessTokenResponse = identityProvider.login(
				username, password);
		String token = accessTokenResponse.getToken();
		return token;
	}

	@Override
	public void logout(String refreshToken) throws ServletException {
		identityProvider.logout(refreshToken);
	}

}
