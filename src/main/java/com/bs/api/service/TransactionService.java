package com.bs.api.service;

import java.util.Date;
import java.util.List;

import com.bs.api.entity.Transaction;

public interface TransactionService {
	
	Transaction create(Transaction transaction);
	
	List<Transaction> findByDateTimeBetween(String accountId, Date startDate, Date endDate);
}
