package com.bs.api.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bs.api.repository.TransactionRepository;
import com.bs.api.entity.Transaction;

@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	TransactionRepository transactionRepository;

	@Override
	public Transaction create(Transaction transaction) {
		return transactionRepository.save(transaction);
	}

	@Override
	public List<Transaction> findByDateTimeBetween(String accountId, Date startDate, Date endDate) {
		return transactionRepository.findByAccountIdAndDateTimeBetween(accountId, startDate, endDate);
	}

}
