package com.bs.util;

public class BankCalculation {

	static final long interestRate = Double.valueOf(3.5).longValue();

	public static long calculateInterestAnnually(long balance) {
		return balance * interestRate / 100;
	}

}
