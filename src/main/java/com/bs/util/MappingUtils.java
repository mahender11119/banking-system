package com.bs.util;

import com.bs.api.entity.Customer;
import com.bs.api.entity.Employee;
import com.bs.api.request.CustomerRequest;
import com.bs.api.request.EmployeeRequest;

public class MappingUtils {

	public static Customer customerRequestToCustomer(
			CustomerRequest customerRequest) {
		Customer customer = new Customer();
		customer.setCity(customerRequest.getCity());
		customer.setFirstName(customerRequest.getFirstName());
		customer.setLastName(customerRequest.getLastName());
		customer.setState(customerRequest.getState());
		customer.setStreet(customerRequest.getStreet());
		customer.setZip(customerRequest.getZip());
		return customer;
	}

	public static Employee employeeRequestToEmployee(
			EmployeeRequest employeeRequest) {
		Employee employee = new Employee();
		employee.setJobRole(employeeRequest.getJobRole());
		employee.setName(employeeRequest.getName());
		employee.setUserName(employeeRequest.getUsername());
		employee.setSalary(employeeRequest.getSalary());
		return employee;
	}
}
